################################################################################################
##############################              Futuros                  ###########################
################################################################################################
rm(list = ls()[!ls() %in% c(Guardar_princial,as.vector(lsf.str()))])
### ---------- Lectura de datos: Bloomberg  -------------- ###  
Almacenar_mercado(Excel = paste0(path,"/Inputs/Econometria/Futuros/Futuros.xlsx"),Output = paste0(path,'/Inputs/Econometria/Futuros/Futuros.Rdata'))

################## --------- Organización de datos: Futuros -------------- ################ 
load(paste0(path,'/Inputs/Econometria/Futuros/Futuros.Rdata'))


### ---- Cambiar estructura de los Nodos del Brent ------- ### 
for (Hoja in 1:length(Base_datos$Datos)) {
  print(paste0("Limpiando la hoja ",Hoja))
  ### --- re nombrando variables --- ### 
  Base_datos$Datos[[Hoja]] = as.data.frame(melt(Base_datos$Datos[[Hoja]][-1,-c(1:2)],id.vars = '...3'))
  Base_datos$Datos[[Hoja]]= Base_datos$Datos[[Hoja]][,c(2,1,3)]
  colnames(Base_datos$Datos[[Hoja]]) = c('Ticker','Fecha','Precio')
  Base_datos$Datos[[Hoja]]$Ticker = as.character(Base_datos$Datos[[Hoja]]$Ticker)

  if (class(Base_datos$Datos[[Hoja]]$Fecha)[[1]] == "character"){
    Base_datos$Datos[[Hoja]]$Fecha = as.Date(as.numeric(Base_datos$Datos[[Hoja]]$Fecha), origin = "1899-12-30")
  } else {
    Base_datos$Datos[[Hoja]]$Fecha = as.Date((Base_datos$Datos[[Hoja]]$Fecha))
  }
  ###--- Limpiar los NA --- ###
  Base_datos$Datos[[Hoja]]$Precio[Base_datos$Datos[[Hoja]]$Precio == "#N/A N/A"] = 0
  Base_datos$Datos[[Hoja]]$Precio = as.numeric(Base_datos$Datos[[Hoja]]$Precio)
  # Base_datos$Datos[[Hoja]]$Precio[Base_datos$Datos[[Hoja]]$Precio == 0] = NA
  }


########## ---------------------------- Last ------------------------- ##########
Precios = list.rbind(Base_datos$Datos)
### ------------------ Curva TRM ---------------- ### 
Precios[,1] = gsub(x = Precios[,1],pattern = '\\+',replacement = "")
Precios[,1] = gsub(x = Precios[,1],pattern = 'Curncy',replacement = "")
Precios[,1] = gsub(x = Precios[,1],pattern = 'CLN',replacement = "TRM_")
### ------------------ Curva Brent ---------------- ### 
Precios[,1] = gsub(x = Precios[,1],pattern = 'Comdty',replacement = "")
Precios[,1] = gsub(x = Precios[,1],pattern = 'CO',replacement = "BRENT_")
### --- Elimina la etiqueta de MES --- ### 
Precios[,1] = gsub(x = Precios[,1],pattern = 'M[[:space:]]$',replacement = "")
Precios[,1] = gsub(x = Precios[,1],pattern = '2Y[[:space:]]$',replacement = "24")
Precios[,1] = gsub(x = Precios[,1],pattern = '[[:space:]]',replacement = "")



########## ---------------------------- Fechas ------------------------- ##########
Items = as.character(unique(Precios[,1]))
Matriz_last = NULL
for (Item in (Items)){
    print(Item)
    Datos = Fechas_mensuales(Datos = Precios,Nombre = Item,Diferencial = F,Promedio = F)
    Matriz_last = rbind(Matriz_last,Datos)
}

TRM = data.frame(Nombre = "TRM",Actualizacion=as.character(Sys.Date()),Min_fecha=as.character(min(Matriz_last[grep(Matriz_last[,2],pattern = "TRM_1"),1]))
                                          ,Max_fecha = as.character(max(Matriz_last[grep(Matriz_last[,2],pattern = "TRM_1"),1])),Contenido = length(Matriz_last[grep(Matriz_last[,2],pattern = "TRM_1"),1]),
                                          Frecuencia = as.character(periodicity(as.Date(Matriz_last[grep(Matriz_last[,2],pattern = "TRM_1"),1]))[6]),Categoria = "Mercado")
Brent = data.frame(Nombre = "Brent",Actualizacion=as.character(Sys.Date()),Min_fecha=as.character(min(Matriz_last[grep(Matriz_last[,2],pattern = "BRENT1"),1]))
                   ,Max_fecha = as.character(max(Matriz_last[grep(Matriz_last[,2],pattern = "BRENT1"),1])),Contenido = length(Matriz_last[grep(Matriz_last[,2],pattern = "BRENT1"),1]),
                   Frecuencia = as.character(periodicity(as.Date(Matriz_last[grep(Matriz_last[,2],pattern = "BRENT1"),1]))[6]),Categoria = "Mercado")



Futuros_datos = rbind(TRM,Brent)
save(Futuros_datos,file = paste0(path,'/Outputs/Matriz/Informacion_futuros.Rdata'))

Matriz_last = as.data.frame.matrix(xtabs(Matriz_last[,c(grep(colnames(Matriz_last),patter = "Precio"))]~Matriz_last[,c(grep(colnames(Matriz_last),patter = "Producto"))]+Matriz_last[,c(grep(colnames(Matriz_last),patter = "Fecha"))]))
Matriz_last = Matriz_last[c(match(Items,rownames(Matriz_last))),]
Matriz_last = Matriz_last[,!apply(Matriz_last, 2, function(x) all(is.na(x)))]
Matriz_last = Matriz_last[!rownames(Matriz_last) == "NA",]
save(Matriz_last,file = paste0(path,'/Outputs/Matriz/Futuros/Matriz_last.Rdata'))

