#############################################################################################
###################################        Econometria      #################################
#############################################################################################
rm(list=ls())
path = '/Users/germangonzalez/Dropbox (Quantil)/Home Center/'
### --- Parametros ---- ### 

### --- Fecha inicial --- ###
Fecha_analisis = '2018-06-01'
### --- Numero se simulaciones --- ###
Simulaciones = 1000
### --- Fecha inicial --- ###
Ajustar_simulaciones = T

### --- TRUE: Ajusta futuros para las series que tengan futuros, y ajusta proyecciones a las que no tengan futuros 
### --- FALSE: Ajusta todas las series a proyecciones --- ## 
Media_futuros = T


########## ---------------------------- Funciones ------------------------- ########## 
source(paste0(path,"/Codigo/Utilities/Funciones.R"))

### --- Vector de variables de control que sobreviven a lo largo del programa --- ## 
Guardar_princial = c("path","Fecha_analisis","Ajustar_simulaciones","Media_futuros","Simulaciones","Guardar_princial")
### Variables de cada empresa ###
Informacion_empresas <<- as.data.frame(read_excel(paste0(path,"/Inputs/Parametros/Parametros.xlsx"),sheet = "Variables"))
Parametros_empresas <<- c("Parametros","inputsReficar","param_hocol","Lista_Inputs_CENIT","Lista_Inputs_OCENSA","Exceles_Ho","ListaInsumos_esenttia")
Variables_guardar <<- c(Guardar_princial,"Fecha_analisis","Modo_backup","A1","path","Base_excel","Matriz_tiempo","Filiales_correr","Empresas","Simulaciones","Matriz_simulaciones","Variables_guardar","TipoProduccion","Parametros_empresas",Parametros_empresas,"Informacion_empresas",c(as.character(Informacion_empresas[,"Insumos"])),c(as.character(Informacion_empresas[,"Cambios"])),as.vector(lsf.str()))


########## ---------------------------- Datos ------------------------- ########## 
source(paste0(path,"/Codigo/Datos/Datos.R"))

########## ---------------------------- Parametros ------------------------- ########## 
source(paste0(path,"/Codigo/Datos/Parametros.R"))

########## ---------------------------- Econometria ------------------------- ########## 
source(paste0(path,"/Codigo/Econometria/Econometria.R"))



