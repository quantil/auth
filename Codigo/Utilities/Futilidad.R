library(nleqslv)
### --- Parametro --- ### 
Norm1=rnorm(10000,0.2,0.1)
Equiv0 = 0.05
Ut = function(retorno,param){((retorno)^(1-param)-1)/(1-param)}

CalcularAversion1 = function(Equiv0){
  
  #     #Funcion que calibra la funcion de utilidad anterior a loterias equivalentes segun el nivel de PyG.
  fun_resolver <- function(param,Equiv0){
    return(UtilidadPyG(1+Norm1,param)-UtilidadPyG(1+Equiv0,param))
  }
  return(nleqslv(ifelse(Equiv0>0,10,0), fun_resolver,Equiv0=Equiv0 ,control=list(ftol=1e-14))$x)
  #-0.005013
}

UtilidadPyG = function(DistribucionPyG,param){
  a=(((DistribucionPyG)^(1-param)-1)/(1-param))
  return(mean(a))
}

A1=CalcularAversion1(0.1)
E0<<-function(x,param){return(((1-param)*x+1)^(1/(1-param)))}

Norm1=rnorm(10000,4.5,3.3)


