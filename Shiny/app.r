################### -------------- Run: QuantRisk --------------  ################### 
require(shiny)
### --- Path inst/application --- ### 
folder_address = '/home/german/quantrisk/Shiny/QuantRisk/inst/application'
runApp(appDir =folder_address, port = 5431,
       launch.browser = getOption("shiny.launch.browser", interactive()),
       host = getOption("shiny.host", "127.0.0.1"), workerId = "",
       quiet = FALSE, display.mode = c("auto", "normal", "showcase"),
       test.mode = getOption("shiny.testmode", FALSE))
################### -------------------------------------------  ################### 